console.log("Hello World!")

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function getUserInfo(){
		let getName = prompt("What is your name?");
		let getAge = prompt("How old are you?");
		let getAddress = prompt("Where do you live?");
		alert("Thank you for your input!");

		console.log("Hello, " + getName);
		console.log("You are " + getAge + " years old.");
		console.log("You live in "+ getAddress);

	}

	getUserInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function myFavBands(){
		console.log("1. Destiny's Child");
		console.log("2. Aerosmith");
		console.log("3. The Calling");
		console.log("4. Kamikazee");
		console.log("5. South Border");
	}

	myFavBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function myFavMovies(){
		console.log("1.Titanic");
		console.log("Rotten Tomatoes Rating: 88%");
		console.log("2.The Da Vinci Code");
		console.log("Rotten Tomatoes Rating: 25%");
		console.log("3.Angel's and Demons");
		console.log("Rotten Tomatoes Rating: 37%");
		console.log("4.Harriet");
		console.log("Rotten Tomatoes Rating: 74%");
		console.log("5.Me before you");
		console.log("Rotten Tomatoes Rating: 54%");
	}
	myFavMovies();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

//printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let firstFriend = prompt("Enter your first friend's name:"); 
	let secondFriend = prompt("Enter your second friend's name:"); 
	let thirdFriend = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(firstFriend); 
	console.log(secondFriend); 
	console.log(thirdFriend); 
};

printFriends();

// console.log(friend1);
// console.log(friend2);